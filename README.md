# VoteLog Content

This repository holds all editorial content of the VoteLog [frontend application](https://gitlab.com/votelog/web).

- All content is written in `Markdown`-Syntax and rendered in the frontend.
	- The parser used in the frontend is [markdown-it](https://github.com/markdown-it/markdown-it) – through the [markdownit-module](https://github.com/nuxt-community/markdownit-module) – and follows the [CommonMark spec](https://spec.commonmark.org/), which can also be extended.
- All four languages need to be created for each page i.e. `de.md`, `en.md`, `fr.md`, and `it.md`.

## Pages

The `pages` directory holds the following pages:
- `/` – or `root` or `index`  – page. Example: `/de`
- `mission/` pages. Example: `/de/mission` or `/de/mission/team`, or more:
	- the `mission/` directory additionally supports [Unknown Dynamic Nested Routes](https://nuxtjs.org/docs/features/file-system-routing/#unknown-dynamic-nested-routes), but this requires additional setup:
		- all string translations of the route names need to be registered in the `lang/` files in the [frontend repository](https://gitlab.com/votelog/web/-/tree/main/lang).
- `method/` page. Example: `/de/methode`
- `imprint/` page. Example: `/de/impressum`
- `privacy/` page. Example: `/de/datenschutz`

## Images

- Please place all images in the `/img` directory of this repository
- To embed an image use this markdown syntax:
  ```markdown
  ![Organisationen bewerten Abstimmungen im Nationalrat](https://gitlab.com/api/v4/projects/33194923/repository/files/img%2FVoteLog-flow-0.svg/raw)
  ```
- How the URL to images work:
  - Base URL is `https://gitlab.com/api/v4/projects/33194923/repository/files/img%2F`
  - Then the filename, example: `VoteLog-flow-0.svg`
  - And then follow it with `/raw`
  - If you want to create sub-directories in the `/img` directory, use the following:
    - The indicator for the GitLab directory is **not** `/` but the string `%2F`
    - Example structure `https://gitlab.com/api/v4/projects/33194923/repository/files/img%2F{subDirectoryName}%2F{fileName}.{fileType}/raw`

## Files

- Please place all files in the `files` directory of this repository
- To link to a file use this syntax:
  ```markdown
  <a href="https://gitlab.com/api/v4/projects/33194923/repository/files/files%2FfileName.pdf" target="_blank" rel="noopener">LinkTitle</a>
  ```
- Always use the HTML `a` tag, so that the `target="_blank"` can be set
- How the URL to files work:
    - Base URL is `https://gitlab.com/api/v4/projects/33194923/repository/files/files%2F`
    - Then the filename, example: `presentation.pdf`
    - If you want to create sub-directories in the `/files` directory, use the following:
        - The indicator for the GitLab directory is **not** `/` but the string `%2F`
        - Example structure `https://gitlab.com/api/v4/projects/33194923/repository/files/files%2F{subDirectoryName}%2F{fileName}.{fileType}`
