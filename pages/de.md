# Transparenz und Rechenschaft

<p class='prominent' markdown='1'>oder: "Was mached die z'Bern dobe eigentli?"</p>

VoteLog zeigt dir, wie zivilgesellschaftliche Organisationen die Abstimmungen im Schweizer Parlament einordnen. Wähle eine Organisation deines Vertrauens aus und schau dir an, welche Parlamentarier:innen in ihrem Interesse abgestimmt haben – und welche nicht. 

VoteLog ist ein gemeinnütziges Civic-Tech-Projekt. Ermöglicht wurde es durch ehrenamtliches Engagement und eine Förderung des Schweizer Prototype Funds.

## Wie funktioniert's?

Zivilgesellschaftliche Organisationen geben auf VoteLog an, welches Resultat sie sich bei einzelnen Abstimmungen im Nationalrat gewünscht hätten.
\
\
![Organisationen bewerten Abstimmungen im Nationalrat](https://gitlab.com/api/v4/projects/33194923/repository/files/img%2Fflow-1.svg/raw)
\
\
Diese Bewertungen werden dann mit dem tatsächlichen Stimmverhalten der einzelnen Nationalrät:innen verglichen und die Übereinstimmung respektive Abweichung in einer Rangliste dargestellt.
\
\
![Durch die Abstimmungsprotokolle erstellt VoteLog eine Einstufung](https://gitlab.com/api/v4/projects/33194923/repository/files/img%2Fflow-2.svg/raw)
\
\
Mit dieser Rangliste kannst du das tatsächliche Abstimmungsverhalten deiner gewählten Repräsentant:innen beurteilen und vielleicht auch Schlüsse für die nächsten Wahlen ziehen.
\
\
![Durch diese Einstufung können die Parlamentarier:innen beurteilt werden](https://gitlab.com/api/v4/projects/33194923/repository/files/img%2Fflow-3.svg/raw)
\
\
[**Hier**](/de/organisationen) geht's zur Auswahl der Organisationen!


## _Early Beta_

Am 24. Februar 2022 präsentierten wir VoteLog am Demo Day des Prototype Funds zum Abschluss der sechsmonatigen Förderperiode. VoteLog ist nun in der "Early Beta"-Phase, d. h. vieles funktioniert bereits – teils mit Ecken und Kanten – einiges ist aber noch in Entwicklung.

Hast du einen Fehler entdeckt oder ein anderweitiges Problem ausgemacht? Möchtest du bei der Entwicklung mitarbeiten? Bist du Teil einer zivilgesellschaftlichen Organisation und möchtest mit VoteLog Bewertungen vornehmen?

Schick uns eine [Mail](mailto:mail@votelog.ch?subject=VoteLog-Testing)!
