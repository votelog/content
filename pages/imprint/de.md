# Impressum

VoteLog ist ein Open-Source-Projekt des gleichnamigen Vereins:

Verein VoteLog \
c/o Fernando Obieta \
Bahnstrasse 23 \
8400 Winterthur 

[mail@votelog.ch](mailto:mail@votelog.ch)
