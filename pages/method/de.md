# Methode 

## Wie funktioniert VoteLog?

1. Organisationen erfassen ihre Präferenz für eine beliebige Anzahl Nationalratsabstimmungen.

2. VoteLog berechnet die Übereinstimmung dieser Präferenzen mit dem realen Abstimmungsverhalten der einzelnen Nationalrät:innen und erstellt daraus eine Rangliste.

3. Besucher:innen können diese Rangierung auf zwei Arten einsehen:

   * Sie können eine Organisation wählen und erhalten eine Rangliste der Parlamentarier:innen nach Übereinstimmung. Die Rangliste lässt sich nach gewissen Kriterien einschränken (Fraktion, Partei, Kanton), wie auch auf Fraktions- und Parteiebene aggregiert anzeigen.
   * Sie können sich für einzelne Parlamentarier:innen die Übereinstimmung mit jeder auf VoteLog vertretenen Organisation anzeigen lassen.

   In Zukunft wird VoteLog die Möglichkeit bieten, Bewertungen mehrerer Organisationen beliebig zu kombinieren und so organisationsübergreifende Ranglisten zu erstellen.

## Wie kommt die Bewertung zustande?

In der aktuellen Form des Prototypen gilt:

- Alle von einer Organisation bewerteten Abstimmungen werden gleich behandelt, es findet *keine zusätzliche Gewichtung* statt. Eine Schlussabstimmung zählt also genau gleich wie die Abstimmung über einen einzelnen Änderungsantrag.
- Enthaltungen werden gleich behandelt wie eine Stimme gegen die Organisationspräferenz.
- Schliesslich werden für einzelne Parlamentarier:innen nur jene Abstimmungen in der Rangierung berücksichtigt, bei welchen sie tatsächlich die Chance hatten, anwesend zu sein: War eine Politiker:in zum Abstimmungszeitpunkt nicht Teil des Rates oder *entschuldigt* abwesend (<a href="https://www.fedlex.admin.ch/eli/cc/2003/514/de#art_57" target="_blank">GRN Art. 57 Abs. 4 Ziff. e</a>), hat die entsprechende Abstimmung keinen Einfluss auf ihre Rangierung.

Die Organisationswertung für einzelne Parlamentarier:innen ergibt sich wie folgt:

$$ n = \frac{N_\textnormal{übereinstimmung}}{N_\textnormal{total}} $$

$$ \sigma(n) = \frac{1}{1+e^{-n}} $$

Wobei $N_\textnormal{total}$ die Anzahl aller Abstimmungen ist, welche von der Organisation bewertet wurden und an welchen die betreffende Parlamentarierin zugleich tatsächlich die Chance hatte, teilzunehmen, und $N_\textnormal{übereinstimmung}$ die Anzahl aller Abstimmungen ist, bei denen das Votum der Parlamentarier:innen mit dem von der Organisation gewählten Ergebnis übereinstimmt.

## Spielregeln

Um VoteLog nutzen zu können, muss sich eine interessierte Organisation bei uns melden. Grundsätzlich richtet sich das Angebot in erster Linie an die organisierte **Zivilgesellschaft** im Sinne einer <a href="https://de.wikipedia.org/wiki/Bürgergesellschaft" target="_blank">bürgergesellschaftlichen Definition</a>. Insbesondere aus Transparenzgründen möchten wir aber auch wirtschaftliche Interessenverbände nicht ausschliessen. Dabei bleiben wir **politisch neutral**, verhindern aber zugleich eine kommerzielle Vereinnahmung unserer Plattform durch einzelne Akteur:innen. 

VoteLog ist <a href="https://de.wikipedia.org/wiki/Open_Source" target="_blank">**Open-Source**-Software</a>, die Funktionsweise daher von allen Interessierten unabhängig überprüfbar. Die via VoteLog erzeugten Daten der Parlamentsbewertung werden als **Open Data** frei zur Verfügung gestellt, was es etwa Forscher:innen und Journalist:innen ermöglicht, unabhängig von VoteLog eine weitere Kontextualisierung der Daten vorzunehmen und unsere Arbeit kritisch zu hinterfragen.

## Einschränkungen

- Die Daten beschränken sich zum jetzigen Zeitpunkt auf die 51. Legislaturperiode. In Zukunft wird es möglich sein, legislaturübergreifend Bewertungen vorzunehmen.

- Die einzelnen Parlamentsabstimmungen lassen sich bislang nicht gewichten. Wir arbeiten aber an einer entsprechenden Möglichkeit.

- Aufgrund der Datenlage ist bislang nur eine Bewertung des Stimmverhaltens im National-, nicht aber im Ständerat möglich.

- VoteLog arbeitet retrospektiv, d. h. nur mit Daten aus bereits erfolgten Parlamentsabstimmungen. Damit kann VoteLog für Bürger:innen nur sinnvoll als Hilfestellung dienen in Bezug auf die Wiederwahl bestehender Parlamentarier:innen, nicht aber zur Beurteilung von Neukandidierenden.
