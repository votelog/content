# Mission

## Was wir als Problem sehen

Obwohl die schweizerische Demokratie mit Initiative und Referendum über zwei direktdemokratische Instrumente verfügt, ist sie schwergewichtig eine *repräsentative* Demokratie: Alle 4 Jahre wählt das Stimmvolk seine Vertreter:innen in den National- und Ständerat. In der darauffolgenden Legislaturperiode behandeln sie hunderte Geschäfte und fällen tausende Einzelentscheide. Nur ein kleiner Bruchteil davon wird dem Volk zur Abstimmung unterbreitet.

Kurzum: Der Löwenanteil der Gesetzgebung wird vom Parlament verantwortet.

Das detaillierte Stimmverhalten der einzelnen Parlamentarier:innen ist dabei zwar jederzeit auf <a href="https://www.parlament.ch/de/ratsbetrieb/curia-vista" target="_blank" rel="noopener">Curia Vista</a> einsehbar und auch entsprechende Rohdaten werden von den Parlamentsdiensten veröffentlicht. Allerdings kann die riesige Datenmenge von Laien nur schwer inhaltlich eingeordnet und die einzelnen Abstimmungen zueinander in Bezug gesetzt werden. Bislang existiert keine öffentliche Plattform, die es Bürger:innen ermöglicht, sich ein informiertes Bild des realen Ratsbetriebes zu machen.

## Wie wir es lösen

VoteLog stellt das Fachwissen der Zivilgesellschaft in den Mittelpunkt. 

In der Schweiz gibt es hunderte zivilgesellschaftliche Organisationen und Verbände, die das politische Geschehen sachkundig und genau verfolgen und beeinflussen. Sie nehmen an Vernehmlassungen teil, publizieren Stellungnahmen, leisten Lobby-Arbeit oder ergreifen allenfalls ein Referendum. Sie haben also eine klare Vorstellung vom gesetzgeberischen Soll-Zustand und nicht selten eine eindeutige Haltung gegenüber spezifischen Parlamentsentscheiden.

VoteLog macht diese Positionen der Öffentlichkeit auf einer gemeinsamen Plattform zugänglich, wo zivilgesellschaftliche Organisationen die einzelnen Parlamentsabstimmungen einheitlich bewerten können. Darauf aufbauend wird allen die Möglichkeit geboten, diese Bewertungen in beliebig aggregierter und visuell aufbereiteter Form nachzuvollziehen, zu vergleichen und (in Zukunft) auch zu kombinieren.

## Was uns unterscheidet

Im Gegensatz zu bestehenden Wahlhilfen wie smartvote beruht VoteLog nicht auf einer Selbsteinschätzung der Parlamentarier:innen, sondern auf ihren tatsächlichen Stimmentscheiden. Unsere Plattform orientiert sich also am realen Handeln der Parlamentarier:innen – den harten Daten – und nicht an unverbindlichen Wahlversprechen.

## An wen wir uns richten

VoteLog richtet sich primär an Bürger:innen und Politikinteressierte sowie gemeinnützige Vereine, zivilgesellschaftliche Interessenverbände und Nichtregierungsorganisationen.

Kern VoteLogs ist eine frei zugängliche Open-Source-Webplattform, deren Output insbesondere Forscher:innen und Journalist:innen als Open Data zur Verfügung steht und es diesen damit erleichtert, den repräsentativen Teil der Schweizer Demokratie nachhaltig zu analysieren, zu hinterfragen und weiter zu kontextualisieren.

## Was wir bewirken wollen

Mit dieser öffentlich zugänglichen Plattform schafft VoteLog gleich lange Spiesse für zivilgesellschaftliche Organisationen, insbesondere auch für kleinere Organisationen, die sich den Einkauf teurer Politikanalysen nicht leisten können. VoteLog stärkt damit die Zivilgesellschaft und erhöht gleichzeitig die politische Transparenz und die Rechenschaftspflicht der demokratisch gewählten Repräsentant:innen.

## Wo wir uns positionieren

Wir als Plattformbetreiber:innen bleiben politisch und weltanschaulich neutral und nehmen selbst keine Parlamentsbewertungen vor. Stattdessen stellen wir eine niederschwellige Infrastruktur zur Verfügung, welche es allen erlaubt, solche Bewertungen zu erstellen – sowohl Nichtregierungsorganisationen und gemeinnützigen Vereinen wie auch Berufs- und Wirtschaftsverbänden mit politischen Zielen.

Wir bilden die Vielfalt der Meinungen ab, indem wir die Ansichten und Interessen verschiedenster Akteur:innen einheitlich und damit vergleichbar abbilden. Als gemeinnützige Plattform tragen wir eine Verantwortung gegenüber allen Mitgliedern der Gesellschaft. Es gilt der gleichwertige Zugang für alle demokratischen Akteure, was VoteLog von bestehenden Parlaments-Monitoring-Werkzeugen unterscheidet.
