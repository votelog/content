# Datenschutzerklärung

Diese Datenschutzerklärung informiert über den Umgang mit Personendaten, die wir im Rahmen des Projekts votelog.ch bearbeiten: Welche Personendaten bearbeiten wir wofür, wie und wo? Was sind ihre Rechte als Personen, deren Daten wir bearbeiten?

Votelog.ch richtet sich an die Einwohner:innen der Schweiz und unterliegt dementsprechend dem <a href="https://www.fedlex.admin.ch/eli/cc/1993/1945_1945_1945/de" target="_blank" rel="noopener">(schweizerischen) Datenschutzrecht</a>.

Die Personendaten von Parlamentarier:innen beziehen wir über die von den Parlamentsdiensten der Bundesversammlung angebotenen <a href="https://www.parlament.ch/de/%C3%BCber-das-parlament/fakten-und-zahlen/open-data-web-services" target="_blank" rel="noopener">Web Services</a>.

Von den Besucher:innen von votelog.ch erfassen wir grundsätzlich keine und von den angemeldeten Nutzer:innen nur notwendige Personendaten (Grundsatz der Datensparsamkeit). Wir verwenden keine Tracker.

# Haftung

Obwohl wir mit grösster Sorgfalt auf die Richtigkeit der veröffentlichten Informationen achten, kann hinsichtlich der inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und Vollständigkeit dieser Informationen keine Gewährleistung übernommen werden.

**Wir bieten eine Plattform zur Bewertung durch Dritte an und nehmen selber keine inhaltlichen Wertungen vor. Für die Bewertungen sind alleine die Drittparteien verantwortlich.** Das Zustandekommen der Bewertung kann anhand des <a href="https://gitlab.com/votelog" target="_blank" rel="noopener">öffentlich zugänglichen Quellcodes</a> nachvollzogen werden.
