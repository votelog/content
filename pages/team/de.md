# Team

Die Plattform VoteLog wird vom gleichnamigen Verein betrieben. Vereint werden wir durch unser Interesse am Schweizer Politikgeschehen und dem Ziel, mit VoteLog die Zivilgesellschaft und letzlich die Demokratie in der Schweiz zu stärken.

<div class="team-grid">
<div>

![Portrait Fernando Obieta](https://gitlab.com/votelog/content/-/raw/85a404953e67782c9632ad37c351c5244dff2450/img/portrait_fernando.png)
## Fernando Obieta
Project Lead, Web-Development

</div><div>

![Portrait Raphael Bossard](https://gitlab.com/votelog/content/-/raw/85a404953e67782c9632ad37c351c5244dff2450/img/portrait_raphael.png)
## Raphael Bossard
Lead Backend Development

</div><div>

![Portrait Simon Fischer](https://gitlab.com/votelog/content/-/raw/85a404953e67782c9632ad37c351c5244dff2450/img/portrait_simon.png)
## Simon Fischer
Lead Frontend Development

</div><div>

![Portrait Patrizia Lenz](https://gitlab.com/votelog/content/-/raw/85a404953e67782c9632ad37c351c5244dff2450/img/portrait_patrizia.png)
## Patricia Lenz
Coordination, Testing

</div><div>

![Portrait Salim Brüggemann](https://gitlab.com/votelog/content/-/raw/85a404953e67782c9632ad37c351c5244dff2450/img/portrait_salim.png)
## Salim Brüggemann
Political Science, Coordination

</div><div>

![Portrait Christoph Lazlo](https://gitlab.com/votelog/content/-/raw/85a404953e67782c9632ad37c351c5244dff2450/img/portrait_christoph.png)
## Christoph Lazlo
Network

</div><div>

![Portrait Reto Schneider](https://gitlab.com/votelog/content/-/raw/85a404953e67782c9632ad37c351c5244dff2450/img/portrait_reto.png)
## Reto Schneider
Database & Parsing

</div>
</div>
